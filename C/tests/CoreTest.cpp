/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include "gtest/gtest.h"
#include "NDTable.h"

/*
#define N_USERTABS 1

static double DS1_data[4]   = { 0.0, 1.0, 2.0, 3.0 };
static double DS1_scale0[2] = { 0.0, 1.0 };
static double DS2_scale1[2] = { 2.0, 3.0 };

static dataset userTabs[N_USERTABS] = {
	{ "usertab.sdf", // filename
	  "/DS1",		 // datasetname
	  2,			 // rank
	  {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1}, // dims
	  1,																 // numel 
	  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // offs
	  DS1_data,															 // data
	  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // scales
	  "Q1",																 // data_quantity
	  "U1",																 // data_unit
	  {"SQ1","SQ2",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // scales_quantities
	  {"SU1","SU2",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // scales_units
	}
};

TEST(CoreTest, findUserTab) {
	//char *scaleQuantities[] = {"SQ1", "SQ2"};
	//char *scaleUnits[]		= {"SU1", "SU2"};
	dataset *ds = NDTable_find_usertab(userTabs, N_USERTABS, "usertab.sdf", "/DS1");
	EXPECT_EQ(&userTabs[0], ds);
}
*/

TEST(CoreTest, setErrorMessage) {
	char msg[MAX_MESSAGE_LENGTH];

	NDTable_set_error_message("%d plus %.1f equals %s", 1, 1.5, "two point five");
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, msg);

	EXPECT_STREQ("1 plus 1.5 equals two point five", msg);
}

TEST(CoreTest, ind2subWithLegalIndex) {
	int subs[3];
	ModelicaNDTable_h ds = NDTable_alloc_table();
	
	ds->ndims = 3;
	ds->offs[0] = 12;
	ds->offs[1] = 4;
	ds->offs[2] = 1;

	NDTable_ind2sub(17, ds, subs);

	EXPECT_EQ(1, subs[0]);
	EXPECT_EQ(1, subs[1]);
	EXPECT_EQ(1, subs[2]);
}

TEST(CoreTest, sub2indWithLegalSubscripts) {
	int subs[3] = { 1, 1, 1 };
	int index;
	ModelicaNDTable_h ds = NDTable_alloc_table();

	ds->ndims = 3;
	ds->dims[0] = 2;
	ds->dims[1] = 3;
	ds->dims[2] = 4;

	NDTable_sub2ind(subs, ds, &index);
	EXPECT_EQ(17, index);
}

TEST(CoreTest, AllocAndFree) {
	ModelicaNDTable_h ds = NULL;
	int i;

	// de-allocate with null pointer
	NDTable_free_table(ds);

	ds = NDTable_alloc_table();

	// make sure all pointers are NULL
	EXPECT_EQ(NULL, ds->filename);
	EXPECT_EQ(NULL, ds->datasetname);
	EXPECT_EQ(NULL, ds->data_quantity);
	EXPECT_EQ(NULL, ds->data_unit);

	// de-allocate with no data
	NDTable_free_table(ds);

	// get a new dataset
	ds = NDTable_alloc_table();

	// allocate some dummy space
	ds->filename = (char *)malloc(1);
	ds->datasetname = (char *)malloc(1);
	ds->data_quantity = (char *)malloc(1);
	ds->data_unit = (char *)malloc(1);
	ds->data = (double *)malloc(1);

	for(i = 0; i < MAX_NDIMS; i++) {
		ds->scales[i] = (double *)malloc(1);
		ds->scale_quantities[i] = (char *)malloc(1);
		ds->scale_units[i] = (char *)malloc(1);
	}

	// de-allocate with data
	NDTable_free_table(ds);
}
