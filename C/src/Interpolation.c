/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include "NDTable.h"

#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifdef _MSC_VER
static const unsigned long __nan[2] = { 0xffffffff, 0x7fffffff };
#define NAN (*(const float *) __nan)
#endif

#ifdef _WIN32
#define ISFINITE(x) _finite(x)
#else
#define ISFINITE(x) isfinite(x)
#endif

// prototype of an interpolation function
typedef int(*interp_fun)(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);

// forward declare inter- and extrapolation functions
static int interp_none		(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);
static int interp_nearest	(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);
static int interp_linear	(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);
static int interp_akima		(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);
static int extrap_hold		(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);
static int extrap_linear	(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]);


void NDTable_find_index(double value, int nvalues, const double *values, int *index, double *t, ModelicaNDTable_ExtrapMethod_t extrap_method) {
	int i;
	double a, b;
	double min = values[0];
	double max = values[nvalues - 1];
	double range = max - min;

	if(nvalues < 2) {
		*t = 0.0;
		*index = 0;
		return;
	}

	// estimate the index and make sure that i <= 0 and i <= 2nd last 
	i = MAX(0, MIN((int)(nvalues * (value - min) / range), nvalues - 2));

	// go up until value < values[i+1]
	while (i < nvalues - 2 && value > values[i+1]) { i++; }

	// go down until values[i] < value
	while (i > 0 && value < values[i]) { i--; }

	a = values[i];
	b = values[i+1];

	*t = (value - a) / (b - a);
	
	*index = i;
}

int NDTable_evaluate(ModelicaNDTable_h table, int nparams, const double params[], ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value) {
	int		 i;
	double	 t	   [MAX_NDIMS]; // the weights for the interpolation
	int		 subs  [MAX_NDIMS];	// the subscripts
	int		 nsubs [MAX_NDIMS];	// the neighboring subscripts
	double	 derivatives [MAX_NDIMS];

	// TODO: add null check

	// if the dataset is scalar return the value
	if (table->ndims == 0) {
		*value = table->data[0];
		return NDTABLE_INTERPSTATUS_OK;
	}

	// find entry point and weights
	for (i = 0; i < table->ndims; i++) {
		NDTable_find_index(params[i], table->dims[i], table->scales[i], &subs[i], &t[i], extrap_method);
	}

	return NDTable_evaluate_internal(table, t, subs, nsubs, 0, interp_method, extrap_method, value, derivatives);
}

int NDTable_evaluate_derivative(ModelicaNDTable_h table, int nparams, const double params[], const double delta_params[], ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value) {
	int		 i, err;
	//dataset *ds;
	double	 t[MAX_NDIMS];		// the weights for the interpolation
	int		 subs[MAX_NDIMS];	// the subscripts
	int		 nsubs[MAX_NDIMS];	// the neighboring subscripts
	double	 derivatives[MAX_NDIMS];

	// TODO: add null check

	// if the dataset is scalar return the value
	if (table->ndims == 0) {
		*value = table->data[0];
		return NDTABLE_INTERPSTATUS_OK;
	}

	// find entry point and weights
	for (i = 0; i < table->ndims; i++) {
		NDTable_find_index(params[i], table->dims[i], table->scales[i], &subs[i], &t[i], extrap_method);
	}

	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, 0, interp_method, extrap_method, value, derivatives)) != 0) {
		return err;
	}

	*value = 0.0;

	for (i = 0; i < nparams; i++) {
		*value += delta_params[i] * derivatives[i];
	}

	return 0;
}


int NDTable_evaluate_internal(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double derivatives[]) {

	interp_fun func;

	// check arguments
	if (table == NULL || t == NULL || subs == NULL || nsubs == NULL || value == NULL || derivatives == NULL) {
		return -1;
	}

	if (dim >= table->ndims) {
		*value = NDTable_get_value_subs(table, nsubs);
		return 0;
	}

	// find the right function:
	if(table->dims[dim] < 2) {
		func = interp_none;
	} else if (t[dim] < 0.0 || t[dim] > 1.0) { 
		// extrapolate
		switch (extrap_method) {
			case NDTABLE_EXTRAP_HOLD:	func = extrap_hold;   break;
			case NDTABLE_EXTRAP_LINEAR: func = extrap_linear; break;
			default: return -1;
		}
	} else { 
		// interpolate
		switch (interp_method) {
			case NDTABLE_INTERP_NEAREST: func = interp_nearest; break;
			case NDTABLE_INTERP_LINEAR:  func = interp_linear;  break;
			case NDTABLE_INTERP_AKIMA:   func = interp_akima;   break;
			default: return -1;
		}
	}

	return (*func)(table, t, subs, nsubs, dim, interp_method, extrap_method, value, derivatives);
}

static int interp_none(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double der_values[]) {
	nsubs[dim] = subs[dim]; // always take the left sample value
	der_values[dim] = 0;
	return NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, value, der_values);
}

static int interp_nearest(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double der_values[]) {
	int err;
	nsubs[dim] = t[dim] < 0.5 ? subs[dim] : subs[dim] + 1;
	der_values[dim] = 0;

	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, value, der_values)) != 0) {
		return err;
	}
	
	// if the value is not finite return NAN
	if (!ISFINITE(*value)) {
		*value = NAN;
		der_values[dim] = NAN;
	}

	return 0;
}

static int interp_linear(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double der_values[]) {
	int err;
	double a, b;

	// get the left value
	nsubs[dim] = subs[dim];
	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, &a, der_values)) != 0) {
		return err;
	}

	// get the right value
	nsubs[dim] = subs[dim] + 1;
	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, &b, der_values)) != 0) {
		return err;
	}

	// if any of the values is not finite return NAN
	if (!ISFINITE(a) || !ISFINITE(b)) {
		*value = NAN;
		der_values[dim] = NAN;
		return 0;
	}

	// calculate the interpolated value
	*value = (1 - t[dim]) * a + t[dim] * b;

	// calculate the derivative
	der_values[dim] = (b - a) / (table->scales[dim][subs[dim] + 1] - table->scales[dim][subs[dim]]);

	return 0;
}

static int interp_akima(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double der_values[]) {
	int i, idx, n, sub, err;
	double x[6] = { 0, 0, 0, 0, 0, 0}, y[6] = { 0, 0, 0, 0, 0, 0}, dx, m1[5], dm[4], b[2], c, d, f1[2], f2[2], f12[2], f12_max, wj, a, l, r, lr, la;

	n = table->dims[dim]; // extent of the current dimension
	sub = subs[dim]; // subscript of current dimension

	for (i = 0; i < 6; i++) {
		idx = sub - 2 + i;

		if (idx >= 0 && idx < n) {
			x[i] = table->scales[dim][idx];

			nsubs[dim] = idx;
			if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, &y[i], der_values)) != 0) {
				return err;
			}
		}
	}

	// if any of the values is not finite return NAN
	for (i = 0; i < 6; i++) {
		if (!ISFINITE(y[i])) {
			*value = NAN;
			der_values[dim] = NAN;
			return 0;
		}
	}

	for (i = MAX(0, 2 - sub); i < MIN(5, 1 + n - sub); i++) {
		m1[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i]);
	}

	// pad left
	if (sub < 2) {
		if (sub < 1) {
			m1[1] = 2.0 * m1[2] - m1[3];
		}
		m1[0] = 2.0 * m1[1] - m1[2];
	}

	// pad right
	if (sub > n - 4) {
		if (sub > n - 3) {
			m1[3] = 2.0 * m1[2] - m1[1];
		}
		m1[4] = 2.0 * m1[3] - m1[2];
	}

	for (i = 0; i < 4; i++) {
		dm[i] = fabs(m1[i + 1] - m1[i]);
	}

	for (i = 0; i < 2; i++) {
		f1[i] = dm[i + 2];
		f2[i] = dm[i];
		f12[i] = f1[i] + f2[i];
	}

	// find the maximum in f12
	f12_max = MAX(f12[0], f12[1]);

	for (i = 0; i < 2; i++) {
		b[i] = f12[i] > 1e-9 * f12_max ? (f1[i] * m1[i + 1] + f2[i] * m1[i + 2]) / f12[i] : m1[i+1];
	}

	dx = x[3] - x[2];

	c = (3.0 * m1[2] - 2.0 * b[0] - b[1]) / dx;
	d = (b[0] + b[1] - 2.0 * m1[2]) / (dx * dx);

	wj = t[dim] * dx;


	*value = ((wj * d + c) * wj + b[0]) * wj + y[2];

	a = x[2] + t[dim] * dx;
	l = x[2];
	r = x[3];
	lr = l - r;
	la = l - a;

	// TODO: calculated with WolframAlpha.com maybe find a better solution to calculate the derivative
	der_values[dim] = -(b[0]*dx)/lr - (2*c*dx*dx*la)/(lr*lr) - 3*d*dx*dx*dx*la*la/(lr*lr*lr);

	return 0;
}

static int extrap_hold(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double der_values[]) {
	int err;
	nsubs[dim] = t[dim] < 0.0 ? subs[dim] : subs[dim] + 1;
	der_values[dim] = 0;
	
	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, value, der_values)) != 0) {
		return err;
	}
	
	// if the value is not finite return NAN
	if (!ISFINITE(*value)) {
		*value = NAN;
		der_values[dim] = NAN;
	}

	return 0;
}

static int extrap_linear(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double der_values[]) {
	int err;
	double a, b;

	nsubs[dim] = subs[dim];
	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, &a, der_values)) != 0) {
		return err;
	}

	nsubs[dim] = subs[dim] + 1;
	if ((err = NDTable_evaluate_internal(table, t, subs, nsubs, dim + 1, interp_method, extrap_method, &b, der_values)) != 0) {
		return err;
	}

	// if any of the values is not finite return NAN
	if (!ISFINITE(a) || !ISFINITE(b)) {
		*value = NAN;
		der_values[dim] = NAN;
		return 0;
	}

	// calculate the extrapolated value
	*value = (1 - t[dim]) * a + t[dim] * b;

	// calculate the derivative
	der_values[dim] = (b - a) / (table->scales[dim][subs[dim] + 1] - table->scales[dim][subs[dim]]);

	return 0;
}
