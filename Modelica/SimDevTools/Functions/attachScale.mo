within SimDevTools.Functions;
function attachScale
  "Set one dataset as the scale for a dimension of another dataset"
  input String fileName "File Name";
  input String datasetName "Dataset Name";
  input String scaleName "Scale Name";
  input String dimensionName = "" "Dimension Name (optional)";
  input Integer dimension "Dimension Index";
external "C" ModelicaHDF5Functions_attach_scale(fileName, datasetName, scaleName, dimensionName, dimension) annotation (
  Include="#include \"ModelicaHDF5Functions.h\"",
  Library={"ModelicaHDF5Functions", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
end attachScale;
