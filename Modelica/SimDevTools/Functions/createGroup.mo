within SimDevTools.Functions;
function createGroup "Create a group in an HDF5 file"
  input String fileName "File Name";
  input String groupName "Group Name";
  input String comment = "" "Comment (optional)";
  external "C"  ModelicaHDF5Functions_create_group(fileName, groupName, comment) annotation (
  Include="#include \"ModelicaHDF5Functions.h\"",
  Library={"ModelicaHDF5Functions", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
end createGroup;
