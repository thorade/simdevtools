within SimDevTools;
model NDTable
extends Modelica.Blocks.Interfaces.MISO;

parameter String filename = "data.sdf" "File name" annotation (Dialog(enable=readFromFile));
parameter String dataset = "/ds0" "Dataset name" annotation (Dialog(enable=readFromFile));
parameter String dataQuantity = "" "Data quantity";
parameter String dataUnit = "" "Data unit";
parameter String scaleQuantities[nin] = fill("", nin) "Scale quantities";
parameter String scaleUnits[nin] = fill("", nin) "Scale units";
parameter SimDevTools.Types.InterpolationMethod interpMethod=SimDevTools.Types.InterpolationMethod.Linear
    "Interpolation method";
parameter SimDevTools.Types.ExtrapolationMethod extrapMethod=SimDevTools.Types.ExtrapolationMethod.None
    "Extrapolation method";
parameter Boolean share = true "Share the dataset if already loaded";

protected
  SimDevTools.Types.ExternalNDTable externalTable=
      SimDevTools.Types.ExternalNDTable(
      filename,
      dataset,
      nin,
      dataQuantity,
      dataUnit,
      scaleQuantities,
      scaleUnits,
      share);

equation
  when initial() then
    usertab();
  end when;

//assert(not (extrapMethod == HDF5Table.Types.ExtrapolationMethod.None and not
//  (containsValue(datasetID, u) == 0)), getErrorMessage(
//  Modelica.Utilities.Strings.length(message), message));

  //y = 2.1; //evaluate(datasetID, u, interpMethod - 1, extrapMethod - 1);
  y = SimDevTools.Internal.evaluate(
    externalTable,
    u,
    interpMethod,
    extrapMethod);
  annotation (Documentation(info="<html>
  
  <p>The <strong>NDTable</strong> block is a multi-dimensional lookup-table (up to 32 dimensions) that supports various inter- and 
  extrapolation methods.</p>

<h3>Interpolation Methods</h3>
  
<p></p>

<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
  <tr><th>Method</th><th>Description</th></tr>
  <tr><td>nearest</td><td>The nearest value is returned</td></tr>
  <tr><td>linear</td><td>The value is linearly interplated</td></tr>
  <tr><td>akima</td><td>The value is calculated using akima interpolation</td></tr>
  </table>
  
<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
<caption align=\"bottom\">Interpolation Methods</caption>
  <tr>
    <td>
    <img src=\"modelica://HDF5Table/Resources/Images/UsersGuide/interpolation_methods.png\" alt=\"Interpolation Methods\">
    </td>
  </tr>
</table>

<h3>Extrapolation Methods</h3>

<p></p>

<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
  <tr><th>Method</th><th>Description</th></tr>
  <tr><td>none</td><td>An error is raised if the requested value is outside the extent of the table</td></tr>
  <tr><td>hold</td><td>The last value in the respective dimension is returned</td></tr>
  <tr><td>linear</td><td>The value is linearly extrapolated using the last two values</td></tr>
  <tr><td>akima</td><td>The value is calculated using the boundry slope of the akima interpolation</td></tr>
</table>

<h2>Input Formats</h2>
  
<h3>HDF5</h3>

<p>Data provided in HDF5 files must be saved in 64 bit float datasets with exactly one dataset
attached as a dimension scale for every dimension. Datasets used as dimension scales must be one-dimensional and strictly
monotonic increasing and the number of elements must match the extent of the respective dimension. 
All values must be finite (i.e. not NaN, +Inf or -Inf).</p>

<p>Additionally every dataset can have attributes of type STRING named \"QUANTITY\", \"UNIT\", \"DISPLAY_UNIT\" and \"COMMENT\" 
that hold the dataset's physical quantity, unit, display unit and comment. The quantity and unit attributes are
evaluated when the datasets is read and an error is raised if the quantity or unit don't match the expected values.</p>

<p><strong>Hint:</strong> The free <a href=\"http://www.simdevtools.org/\">SDF Editor</a> can be used to view, edit and validate HDF5 files in the above format</p>

<h3>UserTab.h</h3>

<p>The second option to provide data to NDTable is the UserTab.h file. This header file can be placed in the Resources/Include 
directory and is compiled into the simulation executable. It defines a static array of ModelicaNDTable_t (the structure used by 
implementation to store the tables).</p>

<p>The filename and datasetname fields are used to identify the table. When the NDTable is initilizing (and the share flag is set to True) 
it will first check if the requested dataset is provided by the UserTab.h file. Otherwise it will load it from a file. The listing below shows an 
example of an UserTab.h file.</p>


<p><strong>Hint:</strong> The python script sdf2usertab.py can be used to generate a header file from an HDF5 file</p>

<pre>
#include \"ModelicaNDTable.h\"

#define N_USERTABS 1

static double DS1_data[4]   = { 0.0, 1.0, 2.0, 3.0 };
static double DS1_scale0[2] = { 0.0, 1.0 };
static double DS2_scale1[2] = { 2.0, 3.0 };

static ModelicaNDTable_t userTabs[N_USERTABS] = {
        { \"usertab.sdf\", // filename
          \"/DS1\",        // datasetname
          2,             // rank
          {2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // dims
          4,                                                                 // numel 
          {2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // offs
          DS1_data,                                                          // data
          {DS1_scale0,DS2_scale1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // scales
          \"Q1\",                                                            // data_quantity
          \"U1\",                                                            // data_unit
          {\"SQ1\",\"SQ2\",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // scales_quantities
          {\"SU1\",\"SU2\",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // scales_units
        }
};
</pre>


</html>"), Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
          {100,100}}), graphics={
      Rectangle(
        extent={{-76,-26},{80,-76}},
        lineColor={0,0,255},
        fillColor={255,255,0},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-76,24},{80,-26}},
        lineColor={0,0,255},
        fillColor={255,255,0},
        fillPattern=FillPattern.Solid),
      Rectangle(
        extent={{-76,74},{80,24}},
        lineColor={0,0,255},
        fillColor={255,255,0},
        fillPattern=FillPattern.Solid),
      Line(
        points={{-28,74},{-28,-76}},
        color={0,0,255}),
      Line(
        points={{24,74},{24,-76}},
        color={0,0,255})}));
end NDTable;
