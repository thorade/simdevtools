within SimDevTools.Types;
class ExternalHDF5Recorder "External object of Recorder"
  extends ExternalObject;

  function constructor "Initialize recorder"
      input String fileName;
      input Integer numSignals;
      input String signalNames[numSignals];
      input String signalQuantities[numSignals];
      input String signalUnits[numSignals];
      input String signalDisplayUnits[numSignals];
      input String signalComments[numSignals];
      input String sdfFileName;
      output ExternalHDF5Recorder externalHDF5Recorder;
  external"C" externalHDF5Recorder =
      ModelicaHDF5Recorder_open(
          fileName,
          numSignals,
          signalNames,
          signalQuantities,
          signalUnits,
          signalDisplayUnits,
          signalComments,
          sdfFileName) annotation (
  Include="#include \"ModelicaHDF5Recorder.h\"",
  Library={"ModelicaHDF5Recorder", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
  end constructor;

  function destructor "Close recorder"
    input ExternalHDF5Recorder externalRecorder;
  external"C" ModelicaHDF5Recorder_close(externalRecorder) annotation (
  Include="#include \"ModelicaHDF5Recorder.h\"",
  Library={"ModelicaHDF5Recorder", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
  end destructor;

end ExternalHDF5Recorder;
