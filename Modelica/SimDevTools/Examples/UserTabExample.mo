within SimDevTools.Examples;
model UserTabExample
  extends Modelica.Icons.Example;
  NDTable nDTable(
    extrapMethod=SimDevTools.Types.ExtrapolationMethod.Nearest,
    nin=2,
    filename="usertab.sdf",
    dataset="/DS1",
    dataQuantity="Q1",
    dataUnit="U1",
    scaleQuantities={"SQ1","SQ2"},
    scaleUnits={"SU1","SU2"},
    interpMethod=SimDevTools.Types.InterpolationMethod.Linear)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Sources.Clock speed
    annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
  Modelica.Blocks.Sources.Constant voltage(k=0.5)
    annotation (Placement(transformation(extent={{-60,10},{-40,30}})));
equation
  connect(speed.y, nDTable.u[1]) annotation (Line(
      points={{-39,-20},{-26,-20},{-26,-1},{-12,-1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(voltage.y, nDTable.u[2]) annotation (Line(
      points={{-39,20},{-26,20},{-26,1},{-12,1}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
        graphics),
    experiment(
      StopTime=10,
      Interval=1,
      __Dymola_fixedstepsize=0.1,
      __Dymola_Algorithm="Euler"),
    __Dymola_experimentSetupOutput);
end UserTabExample;
