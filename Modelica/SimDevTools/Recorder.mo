within SimDevTools;
model Recorder
  parameter Boolean useExternalTrigger=false;
  parameter Modelica.SIunits.Time samplePeriod(min=100*Modelica.Constants.eps,
      start=0.1) = 0.1 "Sample period of component"
    annotation (Dialog(enable=not useExternalTrigger));
  parameter Modelica.SIunits.Time startTime=0 "First sample time instant"
    annotation (Dialog(enable=not useExternalTrigger));
  parameter Integer nin=1 "Number of inputs";
  parameter String fileName="result.h5";
  parameter Boolean convertToSDF=true "Convert result to SDF";
  parameter String sdfFileName="result.sdf" "SDF file name"
    annotation (Dialog(enable=convertToSDF));
  parameter String signalNames[nin]=fill("", nin) "Signal names";
  parameter String signalQuantities[nin]=fill("", nin) "Signal quantities";
  parameter String signalUnits[nin]=fill("", nin) "Signal units";
  parameter String signalDisplayUnits[nin]=fill("", nin) "Signal display units";
  parameter String signalComments[nin]=fill("", nin) "Signal comments";

  Modelica.Blocks.Interfaces.RealInput u[nin] "Connector of Real input signals"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}},
          rotation=0)));

  Modelica.Blocks.Interfaces.BooleanInput trigger if useExternalTrigger
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-100}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-120})));

protected
  SimDevTools.Types.ExternalHDF5Recorder externalRecorder=
      SimDevTools.Types.ExternalHDF5Recorder(
      fileName,
      nin,
      signalNames,
      signalQuantities,
      signalUnits,
      signalDisplayUnits,
      signalComments,
      sdfFileName) "External recorder object";

  Modelica.Blocks.Interfaces.BooleanInput _trigger annotation (Placement(
        transformation(
        extent={{-2,-2},{2,2}},
        rotation=90,
        origin={0,-60})));
  output Boolean sampleTrigger if not useExternalTrigger
    "True, if sample time instant";
  output Boolean firstTrigger if not useExternalTrigger
    "Rising edge signals first sample instant";

equation
  if not useExternalTrigger then
    sampleTrigger = sample(startTime, samplePeriod);

    when sampleTrigger then
      firstTrigger = time <= startTime + samplePeriod/2;
    end when;

    _trigger = sampleTrigger;
  end if;

  when _trigger then
    SimDevTools.Internal.recordSignals(
      externalRecorder,
      time,
      nin,
      u);
  end when;
annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
          -100},{100,100}}), graphics), Icon(coordinateSystem(
        preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
        extent={{-100,100},{100,-100}},
        lineColor={0,0,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid), Ellipse(
        extent={{-34,34},{36,-34}},
        fillColor={255,0,0},
        fillPattern=FillPattern.Solid,
        pattern=LinePattern.None)}));
end Recorder;
