within SimDevTools.Internal.Tests;
model ErrorMessageTest
  extends Modelica.Icons.Example;

  ErrorMessageBlock errorMessageBlock
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end ErrorMessageTest;
