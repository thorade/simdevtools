% Copyright (C) 2014 Modelon GmbH. All rights reserved.
%
% This file is part of the Simulation Development Tools.
%
% This program and the accompanying materials are made
% available under the terms of the BSD 3-Clause License
% which accompanies this distribution, and is available at
% http://simdevtools.org/LICENSE.txt
%
% Contributors:
%   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

% create a scale
ds1 = SDF.Dataset();
ds1.name = 'time';
ds1.comment = 'Time';
ds1.data = 0:0.5:10;
ds1.scale_name = 'Time';
ds1.quantity = 'Time';
ds1.unit = 's';

% create a dataset
ds2 = SDF.Dataset();
ds2.name = 'u';
ds2.comment = 'Measured voltage';
ds2.data = sin(ds1.data);
ds2.quantity = 'Voltage';
ds2.unit = 'V';
ds2.scales = ds1;

% creat a group
g = SDF.Group();
g.comment = 'An example SDF file';
g.datasets = [ds1 ds2];

% save the group
SDF.save('test.sdf', g)