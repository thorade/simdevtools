% Copyright (C) 2014 Modelon GmbH. All rights reserved.
%
% This file is part of the Simulation Development Tools.
%
% This program and the accompanying materials are made
% available under the terms of the BSD 3-Clause License
% which accompanies this distribution, and is available at
% http://simdevtools.org/LICENSE.txt
%
% Contributors:
%   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

function ds = load(filename, datasetname)

file_id = H5F.open(filename, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');

ds_id = H5D.open(file_id, datasetname);

ds = SDF.Dataset(datasetname);

[~,~,ds] = H5A.iterate(ds_id, 'H5_INDEX_CRT_ORDER', 'H5_ITER_NATIVE', 0, @iterate_attributes, ds);

ds.data = H5D.read(ds_id, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');

ds.data = col2row(ds.data);

if ds.rank > 0
    for dim = 1:ds.rank
        [~,~,s] = H5DS.iterate_scales(ds_id, dim-1, [], @iterate_scales, filename);
        if isempty(ds.scales), ds.scales = s; else ds.scales(dim) = s; end 
    end
end

H5F.close(file_id);

end

function [status, s] = iterate_scales(~, ~, scale_id, ~)
    dsname = H5I.get_name(scale_id);
    
    scalename = H5DS.get_scale_name(scale_id);

    s = SDF.Dataset(dsname, [], [], [], [], scalename, []);

    s.data = H5D.read(scale_id, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');
    
    [~,~,s] = H5A.iterate(scale_id, 'H5_INDEX_CRT_ORDER', 'H5_ITER_NATIVE', 0, @iterate_attributes, s);
    
    status = 0; % stop after the first scale
end

function [status, ds] = iterate_attributes(obj_id, attr_name, ~, ds)
    attr_id = H5A.open(obj_id, attr_name, 'H5P_DEFAULT');
    attr = H5A.read(attr_id, 'H5ML_DEFAULT')';
    
    switch attr_name
        case 'COMMENT'
            ds.comment = attr;
        case 'UNIT'
            ds.unit = attr;
        case 'DISPLAY_UNIT'
            ds.display_unit = attr;
    end
    
    H5A.close(attr_id);
    
    status = 0;
end