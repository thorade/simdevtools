Scientific Data Format
======================

```
PLEASE NOTE: This document is a draft
```

The Scientific Data Format (SDF) is a hierarchical storage format for multi-dimensional scalar data that allows users to store quantities, units, comments and custom meta-information together with the actual data.

While SDF is an abstract definition this document also describes the concrete implementation based on HDF5.

### Groups

Groups are container objects that can be used to structure complex data structures. Groups can contain other groups, datasets and attributes. The root of a hierarchy is always a group (usually the file itself).


### Datasets

Datasets contain the actual data values. The data values are arrays with 0 (scalars) to 32 dimensions. Datasets can contain attributes. For each dimension the a dataset may have one dimension scale attached. The length of the dimension scales data vector must match the extent of the respective dimension.


### Dimension Scales

Dimension scales are a special case of datasets that represent the scale for a dimension of one or more datasets within the same file. Their data vector must be one-dimensional and have at least one element. The elements of the data vector must be strictly monotonic increasing.


### Attributes

Attributes are name-value pairs of strings that can be used to store meta-information about the associated objects. The value of a

#### Reserved Attributes

Name         | Where | Description
-------------|-------|----------------------------------
COMMENT      | G, D  | Short description of the group or dataset
QUANTITY     | D     | Physical quantity of the value
UNIT         | D     | Physical unit of the datasets value
DISPLAY_UNIT | D     | Physical unit used to display the value

The names for quantities units and display units are the same as in Modelica.


### Object Names

The names of groups, datasets and attributes must only consist of upper and lower case characters (A-Z, a-z), digits (0-9) and underscores (_) and must start with a character.


Example
-------

```
+ engine.sdf
  - COMMENT: "Example consumption map of a combustion engine"
  + tau
    - COMMENT: "Shaft torque"
    - UNIT: "N.m"
    - QUANTITY: "Torque"
    - "Author": "John Doe"
    - value: [50, 100, 150, 180]
  + w
    - COMMENT: "Shaft speed"
    - UNIT: "rad/s"
    - DISPLAY_UNIT: "rpm"
    - QUANTITY: "AngularVelocity"
    - value: [104.72, 209.44, 314,16, 418.88, 523.6, 628.32]
  + F
    - COMMENT: "Specific fuel consumption"
    - UNIT: "kg/s"
    - DISPLAY_UNIT: "g/s"
    - QUANTITY: "MassFlow"
    - SCALES: [/tau, /w]
    - value: [[0.00082, 0.00105, 0.00153, 0.00212, 0.00276, 0.00364],
              [0.00085, 0.00153, 0.00226, 0.00309, 0.00418, 0.00574],
              [NaN,     0.00226, 0.00325, 0.00443, 0.00581, 0.00738],
              [NaN,     NaN,     NaN,     NaN,     0.00785, NaN]]

```


Storing SDF in HDF5 Containers
------------------------------

This section describes the implementation of SDF in HDF5.

- No filters
- No links

### Attributes

- FORTRAN_STRING, scalar
- UTF-8 encoded,
- no line breaks (`\r`, `\n`, `\t`)


### Datasets

-Datasets are represented as HDF5 Datasets
-Datatype FLOAT_64

### Dimension Scales

-using H5HL API
-at most one scale for every dimension
