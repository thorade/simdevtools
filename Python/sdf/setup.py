from distutils.core import setup

setup(name = "sdf",
      version = "0.1.0",
      description = "Scientific Data Format",
      author = "Torsten Sommer",
      author_email = "torsten.sommer@modelon.com",
      url = "www.simdevtools.org",
      license = "Standard 3-clause BSD",
      packages = ['sdf',],
      #long_description = """Really long text here.""" , 
)