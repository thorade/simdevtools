Multi-dimensional Interpolation for Python
==========================================

The NDTable package provides a lookup table to consistently inter- and
extrapolate arrays of arbitrary dimensions using different algorithms:

* Interpolation: `nearest`, `linear`, `akima`
* Extrapolation: `hold`, `linear`

It can handle non-finite numbers (NaN, +Inf, -Inf).


Websites
--------

* Main website: http://www.simdevtools.org
* Source code: http://bitbucket.org/modelon/simdevtools


Prerequisites
-------------

You need, at a minimum:

* Python 2.6, 2.7, 3.2, 3.3, or 3.4
* NumPy 1.6.1 or later


Installation
------------

Via pip (recommended)::

```
pip install ndtable
```
From a release archive or Git checkout::

```
python setup.py build
python setup.py install
```
